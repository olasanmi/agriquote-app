
const routes = [
  {
    path: '/',
    component: () => import('layouts/main.vue'),
    children: [
      { path: '', component: () => import('pages/index.vue') },
      { path: '/recovery', component: () => import('pages/recovery.vue') },
      { path: '/confirm/account/:uuid', component: () => import('pages/recovery.vue') },
      { path: '/quotes/client/response/:uuid', component: () => import('pages/clientResponse.vue') },
    ]
  },
  {
    path: '/dashboard',
    component: () => import('layouts/dashboard/main.vue'),
    children: [
      { path: '', component: () => import('pages/dashboard/main.vue'), meta: { auth: true } },

      { path: 'users', component: () => import('pages/dashboard/users/index.vue'), meta: { auth: true } },
      { path: 'users/create', component: () => import('pages/dashboard/users/add.vue'), meta: { auth: true } },
      { path: 'users/edit/:id', component: () => import('pages/dashboard/users/add.vue'), meta: { auth: true } },

       { path: 'profile/:id', component: () => import('pages/dashboard/users/profile.vue'), meta: { auth: true } },

      { path: 'clients', component: () => import('pages/dashboard/clients/index.vue'), meta: { auth: true } },
      { path: 'clients/create', component: () => import('pages/dashboard/clients/add.vue'), meta: { auth: true } },
      { path: 'clients/edit/:id', component: () => import('pages/dashboard/clients/add.vue'), meta: { auth: true } },

      { path: 'dealers', component: () => import('pages/dashboard/dealers/index.vue'), meta: { auth: true } },
      { path: 'dealers/create', component: () => import('pages/dashboard/dealers/add.vue'), meta: { auth: true } },
      { path: 'dealers/edit/:id', component: () => import('pages/dashboard/dealers/add.vue'), meta: { auth: true } },

      { path: 'items', component: () => import('pages/dashboard/items/index.vue'), meta: { auth: true } },
      { path: 'items/create', component: () => import('pages/dashboard/items/add.vue'), meta: { auth: true } },
      { path: 'items/edit/:id', component: () => import('pages/dashboard/items/add.vue'), meta: { auth: true } },

      { path: 'quotes', component: () => import('pages/dashboard/quote/index.vue'), meta: { auth: true } },
      { path: 'quotes/create', component: () => import('pages/dashboard/quote/add.vue'), meta: { auth: true } },
      { path: 'quotes/edit/:id', component: () => import('pages/dashboard/quote/add.vue'), meta: { auth: true } },

      { path: 'providers', component: () => import('pages/dashboard/provider/index.vue'), meta: { auth: true } },
      { path: 'providers/create', component: () => import('pages/dashboard/provider/add.vue'), meta: { auth: true } },
      { path: 'providers/edit/:id', component: () => import('pages/dashboard/provider/add.vue'), meta: { auth: true } },

      { path: 'purchase-orders', component: () => import('pages/dashboard/purchaseOrders/index.vue'), meta: { auth: true } },
      { path: 'purchase-orders/create', component: () => import('pages/dashboard/purchaseOrders/add.vue'), meta: { auth: true } },
      { path: 'purchase-orders/edit/:id', component: () => import('pages/dashboard/purchaseOrders/add.vue'), meta: { auth: true } },

      { path: 'trash', component: () => import('pages/dashboard/trash/index.vue'), meta: { auth: true } },

      { path: 'requisitions', component: () => import('pages/dashboard/requisition/index.vue'), meta: { auth: true } },
      { path: 'requisitions/create', component: () => import('pages/dashboard/requisition/add.vue'), meta: { auth: true } },
      { path: 'requisitions/edit/:id', component: () => import('pages/dashboard/requisition/add.vue'), meta: { auth: true } },

      { path: 'products', component: () => import('pages/dashboard/products/index.vue'), meta: { auth: true } },
      { path: 'products/create', component: () => import('pages/dashboard/products/add.vue'), meta: { auth: true } },
      { path: 'products/edit/:id', component: () => import('pages/dashboard/products/add.vue'), meta: { auth: true } },

      { path: 'accesories', component: () => import('pages/dashboard/accesories/index.vue'), meta: { auth: true } },
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/error404.vue')
  }
]

export default routes
