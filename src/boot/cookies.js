import Vue from 'vue'
import VueCookies from 'vue-cookies'

export default async () => {
  Vue.use(VueCookies)
  Vue.$cookies.config('365d')
}
