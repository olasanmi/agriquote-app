import { Notify } from 'quasar'
import axios from 'axios'

export default ({ Vue, router }) => {
  Vue.prototype.$axios = axios;
  axios.interceptors.response.use(response => {
    return response;
  }, error => {
    if (error.response.status === 401) {
      if (error.config.url.split('api').pop() !== '/auth/login') {
        localStorage.removeItem('auth')
        Notify.create({
          message: 'Don`t have authorization for do this request.',
          type: 'negative',
          color: 'red-9',
          position: 'bottom-right'
        })
        localStorage.removeItem('user')
        localStorage.removeItem('token')
        router.push('/')
      }
    }
    return Promise.reject(error);
  });
}
