import formatPrice from 'src/utilities/globalsFunction'
import Vue from 'vue'

export default () => {
  Vue.prototype.$price = formatPrice
}
