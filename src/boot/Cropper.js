// import something here
import Vue from 'vue';
import VueCropper from 'vue-cropperjs';

import 'cropperjs/dist/cropper.css';

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/boot-files
export default async (/* { app, router, Vue ... } */) => {
  Vue.component('vue-cropper', VueCropper);
}
