import getDates from 'src/utilities/date'
import Vue from 'vue'

export default () => {
  Vue.prototype.dateGet = getDates
}
