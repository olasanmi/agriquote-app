import "vue-scroll-picker/dist/style.css"
import VueScrollPicker from "vue-scroll-picker"

export default async ({ Vue }) => {
  Vue.use(VueScrollPicker)
}
