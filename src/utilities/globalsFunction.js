const formatPrice = (total, currency) => {
  return currency + total.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,');
}

export default formatPrice 
