import axios from 'axios'

import { domain } from './data'

var url = domain

export function list (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/purchaseOrder', headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function post (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/purchaseOrder', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error) 
      }
      if (err.response.data.errors) {
        if (err.response.data.errors.date) {
          errorCallBack(err.response.data.errors.date[0])
        }
        if (err.response.data.errors.payment_conditions) {
          errorCallBack(err.response.data.errors.payment_conditions[0])
        }
        if (err.response.data.errors.shipping_conditions) {
          errorCallBack(err.response.data.errors.shipping_conditions[0])
        }
        if (err.response.data.errors.quote_id) {
          errorCallBack(err.response.data.errors.quote_id[0])
        }
        if (err.response.data.errors.provide_id) {
          errorCallBack(err.response.data.errors.provide_id[0])
        }
        if (err.response.data.errors.currency) {
          errorCallBack(err.response.data.errors.currency[0])
        }
      }
    })
}

export function show (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/purchaseOrder/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error) 
      }
    })
}

export function editPurchaseOrder (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/purchaseOrder/' + data.uuid, data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'PUT' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error) 
      }
      if (err.response.data.errors) {
        if (err.response.data.errors.date) {
          errorCallBack(err.response.data.errors.date[0])
        }
        if (err.response.data.errors.payment_conditions) {
          errorCallBack(err.response.data.errors.payment_conditions[0])
        }
        if (err.response.data.errors.shipping_conditions) {
          errorCallBack(err.response.data.errors.shipping_conditions[0])
        }
        if (err.response.data.errors.quote_id) {
          errorCallBack(err.response.data.errors.quote_id[0])
        }
        if (err.response.data.errors.provide_id) {
          errorCallBack(err.response.data.errors.provide_id[0])
        }
      }
    })
}

export function del (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/purchaseOrder/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error) 
      }
    })
}

export function showPdf (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/generate-pdf/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function sendPdf (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/send-email/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.error)
      }
    })
}
