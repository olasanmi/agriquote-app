import axios from 'axios'

import { domain } from './data'

var url = domain

export function recoveryPassword (data, callBack, errorCallBack) {
  axios({ url: url + '/api/v1/auth/recovery', data: data, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        if (err.response.data) {
          if (err.response.data.error) {
            errorCallBack(err.response.data.error) 
          }
          if (err.response.data.errors) {
            if (err.response.data.errors.email) {
              errorCallBack(err.response.data.errors.email[0])
            }
            if (err.response.data.errors.username) {
              errorCallBack(err.response.data.errors.username[0])
            }
            if (err.response.data.errors.password) {
              errorCallBack(err.response.data.errors.password[0])
            }
          }
        }
      }
    })
}

export function logout (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/auth/logout', headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.error)
      }
    })
}


export function login (data, callBack, errorCallBack) {
  axios({ url: url + '/api/v1/auth/login', data: data, headers: { 'Accept': 'application/json;charset=utf-8' }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        if (err.response.data) {
          if (err.response.data.error) {
            errorCallBack(err.response.data.error) 
          }
          if (err.response.data.errors) {
            if (err.response.data.errors.password) {
              errorCallBack(err.response.data.errors.password[0])
            }
            if (err.response.data.errors.username) {
              errorCallBack(err.response.data.errors.username[0])
            }
          }
        }
      }
    })
}

export function upUsersPhotos (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/profile/photo', data: data, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'multipart/form-data', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        if (err.response.data.error) {
          errorCallBack(err.response.data.error)
        }
        if (err.response.data.errors) {
          if (err.response.data.errors.file) {
            errorCallBack(err.response.data.errors.file[0])
          }
        }
      }
    })
}
