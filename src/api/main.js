import axios from 'axios'

import { domain } from './data'

var url = domain

export function getCount (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/main/count', headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error) 
      }
    })
}
