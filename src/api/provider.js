import axios from 'axios'

import { domain } from './data'

var url = domain

export function list (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/provider' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function post (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/provider', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
      if (err.response.data.errors) {
        if (err.response.data.errors.name) {
          errorCallBack(err.response.data.errors.name[0])
        }
        if (err.response.data.errors.country) {
          errorCallBack(err.response.data.errors.country[0])
        }
        if (err.response.data.errors.manager) {
          errorCallBack(err.response.data.errors.manager[0])
        }
        if (err.response.data.errors.address) {
          errorCallBack(err.response.data.errors.address[0])
        }
        if (err.response.data.errors.phone) {
          errorCallBack(err.response.data.errors.phone[0])
        }
        if (err.response.data.errors.email) {
          errorCallBack(err.response.data.errors.email[0])
        }
      }
    })
}

export function show (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/provider/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function editProvider (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/provider/' + data.uuid, data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'PUT' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
      if (err.response.data.errors) {
        if (err.response.data.errors.name) {
          errorCallBack(err.response.data.errors.name[0])
        }
        if (err.response.data.errors.country) {
          errorCallBack(err.response.data.errors.country[0])
        }
        if (err.response.data.errors.manager) {
          errorCallBack(err.response.data.errors.manager[0])
        }
        if (err.response.data.errors.address) {
          errorCallBack(err.response.data.errors.address[0])
        }
        if (err.response.data.errors.phone) {
          errorCallBack(err.response.data.errors.phone[0])
        }
        if (err.response.data.errors.email) {
          errorCallBack(err.response.data.errors.email[0])
        }
      }
    })
}

export function del (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/provider/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}
