import axios from 'axios'

import { domain } from './data'

var url = domain

export function list (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/item', headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function post (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/item', data: data, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'multipart/form-data', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error) 
      }
      if (err.response.data.errors) {
        if (err.response.data.errors.name) {
          errorCallBack(err.response.data.errors.name[0])
        }
        if (err.response.data.errors.name_en) {
          errorCallBack(err.response.data.errors.name_en[0])
        }
        if (err.response.data.errors.reference) {
          errorCallBack(err.response.data.errors.reference[0])
        }
        if (err.response.data.errors.maker) {
          errorCallBack(err.response.data.errors.maker[0])
        }
        if (err.response.data.errors.quote_number) {
          errorCallBack(err.response.data.errors.quote_number[0])
        }
        if (err.response.data.errors.image) {
          errorCallBack(err.response.data.errors.image[0])
        }
      }
    })
}

export function show (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/item/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error) 
      }
    })
}

export function editItem (id, data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/item/' + id + '?_method=PUT', data: data, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'multipart/form-data', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error) 
      }
      if (err.response.data.errors) {
        if (err.response.data.errors.name) {
          errorCallBack(err.response.data.errors.name[0])
        }
        if (err.response.data.errors.name_en) {
          errorCallBack(err.response.data.errors.name_en[0])
        }
        if (err.response.data.errors.reference) {
          errorCallBack(err.response.data.errors.reference[0])
        }
        if (err.response.data.errors.maker) {
          errorCallBack(err.response.data.errors.maker[0])
        }
        if (err.response.data.errors.quote_number) {
          errorCallBack(err.response.data.errors.quote_number[0])
        }
        if (err.response.data.errors.image) {
          errorCallBack(err.response.data.errors.image[0])
        }
      }
    })
}

export function del (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/item/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error) 
      }
    })
}

export function search (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/search/items', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: 'Bearer ' + newToken }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error) 
      }
    })
}
