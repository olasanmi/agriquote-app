import axios from 'axios'
import { domain } from './data'
var url = domain

export const list = async (data, callBack, errorCallBack) => {
  const token = localStorage.getItem('token')
  await axios({ url: url + `/api/v1/requisition${data}`, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'application/json;charset=utf-8', 'Authorization': `Bearer ${token}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err) {
        errorCallBack(err.response.data)
      }
    })
}

export const save = async (data, callBack, errorCallBack) => {
  const token = localStorage.getItem('token')
  await axios({ url: url + '/api/v1/requisition', data: data, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'application/json;charset=utf-8', 'Authorization': `Bearer ${token}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err) {
        errorCallBack(err.response.data)
      }
    })
}

export const filterRequisitions = async (data, callBack, errorCallBack) => {
  const token = localStorage.getItem('token')
  await axios({ url: url + '/api/v1/requisition/' + data, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'application/json;charset=utf-8', 'Authorization': `Bearer ${token}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err) {
        errorCallBack(err.response.data)
      }
    })
}

export const updateRequisition = async (data, callBack, errorCallBack) => {
  const token = localStorage.getItem('token')
  await axios({ url: url + '/api/v1/requisition/' + data.uuid, data: data, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'application/json;charset=utf-8', 'Authorization': `Bearer ${token}` }, method: 'PUT' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err) {
        errorCallBack(err.response.data)
      }
    })
}

export const deleteRequisition = async (data, callBack, errorCallBack) => {
  const token = localStorage.getItem('token')
  await axios({ url: url + '/api/v1/requisition/' + data, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'application/json;charset=utf-8', 'Authorization': `Bearer ${token}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err) {
        errorCallBack(err.response.data)
      }
    })
}

export const suggestion = async (data, callBack, errorCallBack) => {
  const token = localStorage.getItem('token')
  await axios({ url: url + '/api/v1/filter/silos/models?start=' + data.start + '&end=' + data.end + '&main=' + data.main, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'application/json;charset=utf-8', 'Authorization': `Bearer ${token}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err) {
        errorCallBack(err.response.data)
      }
    })
}

export const deleteSiloRequisition = async (data, callBack, errorCallBack) => {
  const token = localStorage.getItem('token')
  await axios({ url: url + '/api/v1/requisitionSilo/' + data, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'application/json;charset=utf-8', 'Authorization': `Bearer ${token}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err) {
        errorCallBack(err.response.data)
      }
    })
}

export const sendEmailMessage = async (data, callBack, errorCallBack) => {
  const token = localStorage.getItem('token')
  await axios({ url: url + '/api/v1/send/requisition?id=' + data, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'application/json;charset=utf-8', 'Authorization': `Bearer ${token}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err) {
        errorCallBack(err.response.data)
      }
    })
}

export const generateCode = async (data, callBack, errorCallBack) => {
  const token = localStorage.getItem('token')
  await axios({ url: url + '/api/v1/code/requisition?country=' + data, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'application/json;charset=utf-8', 'Authorization': `Bearer ${token}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err) {
        errorCallBack(err.response.data)
      }
    })
}

export function uploadCroquis (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/upload/croquis', data: data, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'multipart/form-data', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        if (err) {
          errorCallBack(err.response.data)
        }
      }
    })
}

export function generatePdf (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/requisition/pdf/generation', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        if (errorCallBack != null) {
          if (err) {
            reject(err.response.data)
          }
        }
      })
  })
}

export function sendFirstEmail (data) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/email/requisition', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        if (err) {
          reject(err.response.data)
        }
      })
  })
}

export function sendOneRequisition (data) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/email/one/requisition', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        if (err) {
          reject(err.response.data)
        }
      })
  })
}

export const listOportunities = async (callBack, errorCallBack) => {
  const token = localStorage.getItem('token')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/oportunities/requisition', headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'application/json;charset=utf-8', 'Authorization': `Bearer ${token}` }, method: 'GET' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function completeRequisitions (data, callBack, errorCallBack) {
  const token = localStorage.getItem('token')
  axios({ url: url + '/api/v1/complete/requisition', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${token}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function emailPlay (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/requisition-email/play', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}


/////////////////mechanical requisition
export function generateMechanicalReference (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/mechanical/reference', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function saveMechanicalRequisition (data) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/requisitionMaterial', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function listMechanicalRequisition (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + `/api/v1/requisitionMaterial${data}`, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
  .then(response => {
    callBack(response)
  })
  .catch(err => {
    errorCallBack(err.response.data)
  })
}

export function deletaMechanicalRequisition (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + `/api/v1/requisitionMaterial/${data}`, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'DELETE' })
  .then(response => {
    callBack(response)
  })
  .catch(err => {
    errorCallBack(err.response.data)
  })
}

export const filterRequisitionsMaterial = async (data) => {
  const token = localStorage.getItem('token')
  return new Promise((resolve, reject) => {
    axios({ url: url + `/api/v1/requisitionMaterial/${data}`, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'application/json;charset=utf-8', 'Authorization': `Bearer ${token}` }, method: 'GET' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function updateMechanicalRequisition (data) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + `/api/v1/requisitionMaterial/${data.uuid}`, data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'PUT' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function generatePdfs (data) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/mechanical/pdf/generation', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function sendEmailsToProviders (data) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/send/email/mechanic/requisition', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data.error)
      })
  })
}

export const listOportunitiesMechanical = async () => {
  const token = localStorage.getItem('token')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/list/tracking/email/mechanical', headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'application/json;charset=utf-8', 'Authorization': `Bearer ${token}` }, method: 'GET' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data.error)
      })
  })
}

export function emailPlayMechanical (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/mechanical/tracking/stop', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function completeMechanical (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/mechanical/tracking/complete', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function sendOneEmailMechanical (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/mechanical/send/one-email', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      resolve(response)
    })
    .catch(err => {
      reject(err.response.data.error)
    })
  })
}
