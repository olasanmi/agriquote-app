import axios from 'axios'

import { domain } from './data'

var url = domain

export function list (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/quote' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function filterQuote (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/get/quote/' + data, headers: { 'Accept': 'application/json;charset=utf-8' }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function post (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/quote', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
      if (err.response.data.errors) {
        if (err.response.data.errors.code) {
          errorCallBack(err.response.data.errors.code[0])
        }
        if (err.response.data.errors.date_creation) {
          errorCallBack(err.response.data.errors.date_creation[0])
        }
        if (err.response.data.errors.date_validation) {
          errorCallBack(err.response.data.errors.date_validation[0])
        }
        if (err.response.data.errors.subtotal) {
          errorCallBack(err.response.data.errors.subtotal[0])
        }
        if (err.response.data.errors.discount) {
          errorCallBack(err.response.data.errors.discount[0])
        }
        if (err.response.data.errors.packaging_value) {
          errorCallBack(err.response.data.errors.packaging_value[0])
        }
        if (err.response.data.errors.freight_value) {
          errorCallBack(err.response.data.errors.freight_value[0])
        }
        if (err.response.data.errors.load_containers) {
          errorCallBack(err.response.data.errors.load_containers[0])
        }
        if (err.response.data.errors.user_id) {
          errorCallBack(err.response.data.errors.user_id[0])
        }
        if (err.response.data.errors.dealer_id) {
          errorCallBack(err.response.data.errors.dealer_id[0])
        }
        if (err.response.data.errors.client_id) {
          errorCallBack(err.response.data.errors.client_id[0])
        }
        if (err.response.data.errors.items) {
          errorCallBack(err.response.data.errors.items[0])
        }
      }
    })
}

export function show (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/quote/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function editQuote (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/quote/' + data.uuid, data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'PUT' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
      if (err.response.data.errors) {
        if (err.response.data.errors.code) {
          errorCallBack(err.response.data.errors.code[0])
        }
        if (err.response.data.errors.date_creation) {
          errorCallBack(err.response.data.errors.date_creation[0])
        }
        if (err.response.data.errors.date_validation) {
          errorCallBack(err.response.data.errors.date_validation[0])
        }
        if (err.response.data.errors.subtotal) {
          errorCallBack(err.response.data.errors.subtotal[0])
        }
        if (err.response.data.errors.discount) {
          errorCallBack(err.response.data.errors.discount[0])
        }
        if (err.response.data.errors.packaging_value) {
          errorCallBack(err.response.data.errors.packaging_value[0])
        }
        if (err.response.data.errors.freight_value) {
          errorCallBack(err.response.data.errors.freight_value[0])
        }
        if (err.response.data.errors.load_containers) {
          errorCallBack(err.response.data.errors.load_containers[0])
        }
        if (err.response.data.errors.user_id) {
          errorCallBack(err.response.data.errors.user_id[0])
        }
        if (err.response.data.errors.dealer_id) {
          errorCallBack(err.response.data.errors.dealer_id[0])
        }
        if (err.response.data.errors.client_id) {
          errorCallBack(err.response.data.errors.client_id[0])
        }
        if (err.response.data.errors.items) {
          errorCallBack(err.response.data.errors.items[0])
        }
      }
    })
}

export function del (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/quote/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function generateReference (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/quote/reference', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: 'Bearer ' + newToken}, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
      if (err.response.data.errors) {
        if (err.response.data.errors.code) {
          errorCallBack(err.response.data.errors.code[0])
        }
      }
    })
}

export function toggleStatus (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/quote/status/change', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
      if (err.response.data.errors) {
        if (err.response.data.errors.quote_statuse_id) {
          errorCallBack(err.response.data.errors.quote_statuse_id[0])
        }
      }
    })
}

export function upBlueprints (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/quote/blueprints', data: data, headers: { 'Accept': 'application/json;charset=utf-8', 'Content-Type': 'multipart/form-data', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        if (err.response.data.error) {
          errorCallBack(err.response.data.error)
        }
        if (err.response.data.errors) {
          if (err.response.data.errors.file) {
            errorCallBack(err.response.data.errors.file[0])
          }
        }
      }
    })
}

export function postComment (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/quote/client/response', data: data, headers: { 'Accept': 'application/json;charset=utf-8' }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function acceptTerms (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/quote/terms/accept', data: data, headers: { 'Accept': 'application/json;charset=utf-8' }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function clearBlueprints (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/quotes/blueprints/delete', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function getOpportunities (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/opportunities/quote', headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function showPdf (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/pdf/quote?uuid=' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function delSubitem (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/ItemQuoteSubitem/' + data + '?subitem=' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function delItem (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/QuoteItem/' + data + '?item=' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function showByRefs (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/get/quote?code=' + data + '&load=1', headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function completeQuote (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/complete/quote', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function removeOportunities (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/opportunities/remove', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function changeInterval (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/interval/quote', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function quoteEmailPlay (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/email/play', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function listTrashed (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/quotes/trashed', headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function restoreQuote (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/restore/trashed', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function delOpportunity (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/opportunities/delete/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function sendEmail (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/send/email', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (err.response.data.error) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function listComplete() {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/quote/get/completes', headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        if (err.response.data.error) {
          reject(err.response.data.error)
        }
      })
  })
}
