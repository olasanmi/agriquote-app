import axios from 'axios'

import { domain } from './data'

var url = domain

export function listAccesory () {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/accesory', headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function addAccesory (params) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/accesory', data: params, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function showAccesory (id) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + `/api/v1/accesory/${id}`, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function editAccesory (params) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + `/api/v1/accesory/${params.uuid}`, data: params, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'PUT' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function deleteAccesory (id) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + `/api/v1/accesory/${id}`, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'DELETE' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}
