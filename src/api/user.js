import axios from 'axios'

import { domain } from './data'

var url = domain

export function list (callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/users', headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function add (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/users', data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        if (err.response.data.error) {
          errorCallBack(err.response.data.error) 
        }
        if (err.response.data.errors) {
          if (err.response.data.errors.name) {
            errorCallBack(err.response.data.errors.name[0])
          }
          if (err.response.data.errors.email) {
            errorCallBack(err.response.data.errors.email[0])
          }
          if (err.response.data.errors.password) {
            errorCallBack(err.response.data.errors.password[0])
          }
          if (err.response.data.errors.user_type_id) {
            errorCallBack(err.response.data.errors.user_type_id[0])
          }
        }
      }
    })
}

export function deleteUser (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/users/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'DELETE' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function showUser (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/users/' + data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.error)
      }
    })
}

export function edit (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/users/' + data.uuid, data: data, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'PUT' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        if (err.response.data.error) {
          errorCallBack(err.response.data.error) 
        }
        if (err.response.data.errors) {
          if (err.response.data.errors.name) {
            errorCallBack(err.response.data.errors.name[0])
          }
          if (err.response.data.errors.email) {
            errorCallBack(err.response.data.errors.email[0])
          }
          if (err.response.data.errors.password) {
            errorCallBack(err.response.data.errors.password[0])
          }
          if (err.response.data.errors.user_type_id) {
            errorCallBack(err.response.data.errors.user_type_id[0])
          }
        }
      }
    })
}

export function changeForUser (data, callBack, errorCallBack) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  axios({ url: url + '/api/v1/confirm/user', data: data, headers: { 'Accept': 'application/json;charset=utf-8' }, method: 'POST' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        if (err.response.data.error) {
          errorCallBack(err.response.data.error) 
        }
        if (err.response.data.errors) {
          if (err.response.data.errors.password) {
            errorCallBack(err.response.data.errors.password[0])
          }
        }
      }
    })
}
