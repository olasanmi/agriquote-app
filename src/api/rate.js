import axios from 'axios'

export function getExchangeRate (callBack, errorCallBack) {
  axios({ url: 'https://api.frankfurter.app/latest?to=USD', method: 'GET' })
    .then(response => {
      callBack(response)
    })
    .catch(err => {
      if (errorCallBack != null) {
        errorCallBack(err.response.data.error)
      }
    })
}
