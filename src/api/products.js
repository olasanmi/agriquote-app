import axios from 'axios'

import { domain } from './data'

var url = domain

export function listProduct (query = '') {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + `/api/v1/product?${query}`, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function addProduct (params) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + '/api/v1/product', data: params, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'POST' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function showProduct (id) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + `/api/v1/product/${id}`, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'GET' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function editProduct (params) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + `/api/v1/product/${params.uuid}`, data: params, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'PUT' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}

export function deleteProduct (id) {
  var token = localStorage.getItem('token') || ''
  const newToken = token.replace('"', ' ')
  return new Promise((resolve, reject) => {
    axios({ url: url + `/api/v1/product/${id}`, headers: { 'Accept': 'application/json;charset=utf-8', Authorization: `Bearer ${newToken}` }, method: 'DELETE' })
      .then(response => {
        resolve(response)
      })
      .catch(err => {
        reject(err.response.data)
      })
  })
}
