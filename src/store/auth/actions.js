import { login, logout, recoveryPassword, upUsersPhotos } from 'src/api/auth'

export function loginUser ({ commit }, data) {
  localStorage.removeItem('user')
  localStorage.removeItem('token')
  return new Promise((resolve, reject) => {
    return login(data, (response) => {
      if (response.data.success.id) {
        localStorage.setItem('user', JSON.stringify(response.data.success))
        localStorage.setItem('token', response.data.success.token)
      }
      commit('setAuth', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function closeSession ({ commit }) {
  return new Promise((resolve, reject) => {
    return logout((response) => {
      localStorage.removeItem('user')
      localStorage.removeItem('token')
      commit('logout')
      resolve()
    }, (err) => {
      reject(err)
    })
  })
}

export function changePassword ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return recoveryPassword(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function changePhoto ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return upUsersPhotos(data, (response) => {
      localStorage.removeItem('user')
      localStorage.setItem('user', JSON.stringify(response.data.user))
      commit('setUser', response.data.user)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function setUser ({ commit }, data) {
  return new Promise((resolve, reject) => {
    localStorage.removeItem('user')
    localStorage.setItem('user', JSON.stringify(data))
    commit('setUser', data)
  })
}
