export function setAuth (auth, data) {
  auth.user = data
  auth.token = data.token
  auth.state = true
}

export function setUser (auth, data) {
  auth.user = data
}

export function logout (auth, data) {
  auth.user = {}
  auth.token = null
  auth.state = false
}
