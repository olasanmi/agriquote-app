export function user (auth) {
  return auth.user
}

export function token (auth) {
  return auth.token
}
