import { list, post } from 'src/api/clients'

export function all ({ commit }) {
  return new Promise((resolve, reject) => {
    return list((response) => {
      commit('setClients', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function add ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return post(data, (response) => {
      commit('setClients', [])
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}
