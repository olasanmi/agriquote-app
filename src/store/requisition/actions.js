import { list, save, updateRequisition, deleteRequisition, listMechanicalRequisition, deletaMechanicalRequisition } from 'src/api/requisitions'

export function listRequisitions({ commit }, data) {
  return new Promise((resolve, reject) => {
    return list(data, (response) => {
      commit('setRequisitions', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function listRequisitionsMechanical({ commit }, data) {
  return new Promise((resolve, reject) => {
    return listMechanicalRequisition(data, (response) => {
      commit('setRequisitions', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function saveRequisitions({ commit }, data) {
  return new Promise((resolve, reject) => {
    return save(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function editRequisition({ commit }, data) {
  return new Promise((resolve, reject) => {
    return updateRequisition(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function removeRequisition({ commit }, data) {
  return new Promise((resolve, reject) => {
    return deleteRequisition(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function removeRequisitionMechanical({ commit }, data) {
  return new Promise((resolve, reject) => {
    return deletaMechanicalRequisition(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}
