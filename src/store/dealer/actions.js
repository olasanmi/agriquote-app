import { list, post } from 'src/api/dealer'

export function all ({ commit }) {
  return new Promise((resolve, reject) => {
    return list((response) => {
      commit('setDealers', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function add ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return post(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}
