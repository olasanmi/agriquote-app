import { list, add } from 'src/api/user'

export function all ({ commit }) {
  return new Promise((resolve, reject) => {
    return list((response) => {
      commit('setUsers', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function post ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return add(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}
