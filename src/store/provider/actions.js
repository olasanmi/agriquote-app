import { list, post } from 'src/api/provider'

export function all ({ commit }, query) {
  return new Promise((resolve, reject) => {
    return list(query, (response) => {
      commit('setProviders', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function add ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return post(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}
