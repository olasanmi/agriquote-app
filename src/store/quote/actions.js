import { list, post, showByRefs, listTrashed, listComplete } from 'src/api/quotes'

export function all ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return list(data, (response) => {
      commit('setQuotes', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function allTrashed ({ commit }) {
  return new Promise((resolve, reject) => {
    return listTrashed((response) => {
      commit('setQuotes', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function add ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return post(data, (response) => {
      commit('setQuotes', [])
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function ref ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return showByRefs(data, (response) => {
      commit('setQuote', response.data.success)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function setRate ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setRate', data)
    resolve(data)
  }, (err) => {
    reject(err)
  })
}

export function setTab ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setTab', data)
    resolve(data)
  }, (err) => {
    reject(err)
  })
}

export function setSelectedItem ({ commit }, data) {
  return new Promise((resolve, reject) => {
    commit('setSelectedItem', data)
    resolve(data)
  }, (err) => {
    reject(err)
  })
}

export async function listCompleteQuote ({ commit }) {
  try {
    const { data } = await listComplete()
    if (data.success && data.success.length > 0) {
      commit('completeQuotes', data.success)
    }
    return data
  } catch (error) {
    console.log(error)
  }
}
