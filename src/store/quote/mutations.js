export function setQuotes (quote, data) {
  quote.quotes = data
}

export function setQuote (quote, data) {
  quote.quote = data
}

export function completeQuotes (quote, data) {
  quote.completeQuotes = data
}

export function setRate (quote, data) {
  quote.exchangeRate = data
}

export function setSelectedItem (quote, data) {
  quote.itemSelected = data
}

export function setTab (quote, data) {
  quote.tab = data
}
