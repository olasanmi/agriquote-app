export function quotes (quote) {
  return quote.quotes
}

export function quote (quote) {
  return quote.quote
}

export function rate (quote) {
  return quote.exchangeRate
}

export function itemSelected (quote) {
  return quote.itemSelected
}

export function tab (quote) {
  return quote.tab
}

export function completeQuotes(state) {
  return state.completeQuotes
}
