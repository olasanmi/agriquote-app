import Vue from 'vue'
import Vuex from 'vuex'

import auth from './auth'
import client from './client'
import user from './user'
import dealer from './dealer'
import item from './item'
import quote from './quote'
import provider from './provider'
import purchaseOrder from './purchaseOrder'
import requisition from './requisition'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      auth,
      client,
      user,
      dealer,
      item,
      quote,
      provider,
      purchaseOrder,
      requisition
    },

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEBUGGING
  })

  return Store
}
