import { list, post } from 'src/api/item'

export function all ({ commit }) {
  return new Promise((resolve, reject) => {
    return list((response) => {
      commit('setItems', response.data.success.items)
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}

export function add ({ commit }, data) {
  return new Promise((resolve, reject) => {
    return post(data, (response) => {
      resolve(response)
    }, (err) => {
      reject(err)
    })
  })
}
